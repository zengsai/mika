# `Mika`

An experimental opinionated framework for frontend wasm app, base on [`futures-signals`](https://github.com/Pauan/rust-signals) and [`wasm-bindgen`](https://github.com/rustwasm/wasm-bindgen). `Mika` is inspired by [Dominator](https://github.com/Pauan/rust-dominator).

## Compare to Dominator

|                                       | `Dominator`                                                                                                         | `Mika`                                                                                                                                                                                                                                                                |
| ------------------------------------- | ------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Status                                | "*It is generally feature complete [...]*" - Quoted from its README                                                 | experimental. No single line of `#[test]` yet. It's just enough to run a partial version of TodoMVC (no routing, no persistent storage)                                                                                                                               |
| Base on                               | Stdweb (stdweb is now also based on wasm-bindgen)                                                                   | wasm-bindgen directly                                                                                                                                                                                                                                                 |
| Separation of updating and rendering. | By design, no! [But can be easily added](https://github.com/Pauan/rust-dominator/issues/12#issuecomment-495046146). | Yes, it **tries** to help users separating the app-view-renderer and the app-state-updater. It's impossible to prevent users from insert app-state-mutating code into the render, but at least, `Mika` give its users a way to extract such code out of the renderer. |
| DOM contruction                       | html!("div", {.class("some-class"), [...chilren...]})                                                               | Div::new().class("some-class").child(...).child(...)                                                                                                                                                                                                                  |

## Pros and cons

Both biggest advantage and biggest disadvantage are come from `futures-signals`.

Pros
* `futures-signals` helps triggering the right parts of the code to response to a change in your app. (Contrast to virtual DOM approachs, which must walk through the whole virtual DOM to find diffs)

Cons
* You must learn `futures-signals`.
* Because of `futures-signals`, you find yourself fighting the borrow checker more often.

## Build and serve

* You must [install Rust](https://www.rust-lang.org/en-US/)
* `rustup install nightly` (futures-signals build with futures 0.3 which requires nightly)
* `rustup target add wasm32-unknown-unknown --toolchain=nightly` (because we build for the web browser)
* `cargo install wasm-bindgen-cli` (`Mika` build on top of `wasm-bindgen`)
* `cargo install simi-cli` (`Mika` is similar to [`Simi`](https://gitlab.com/limira-rs/simi))
* `git clone https://gitlab.com/limira-rs/mika.git`
* `cd examples/counter`
* `simi serve` (yes, it's `simi serve`, because we use `simi-cli`)

Wait for the build to complete, then open your browser and visit http://localhost:8000/!

## Documentation

***Not completed yet.***

Minor notes:
* You get `signal`/`signal_vec` from an instance of `Mutable<T>`/`MutableVec<T>` where `T: Copy`, 
* You get `signal_cloned`/`signal_vec_cloned` from an instance of `Mutable<T>`/`MutableVec<T>` where `T: Clone`,

### Permitted content of an element

Mika **tries** to help you with the restrictions when adding child-elements to an element. If you find out that it does not allow a valid child or allow an invalid child (or any other bug), feel free to open an issue. Mika is unable to help on other restrictions such as descendant elements (child-elements in a child-element), child-element which is valid/invalid when with/without an attribute, order of child-elements...

There is still a bunch of `TODO` in `src/dom/mod.rs`, contributions are welcome.

### Element attributes
Most of attributes are available via method such as:
```rust
mika::dom::A::new().href(...);
```