use mika::prelude::*;
use wasm_bindgen::prelude::*;

use signals::signal::Mutable;

#[simi_app]
struct Counter {
    value: Mutable<i32>,
}

enum Msg {
    Up,
    Down,
}

impl Application for Counter {
    type Message = Msg;
    fn init() -> Self {
        Self {
            value: Mutable::new(2019),
        }
    }

    fn update(&mut self, m: Self::Message) {
        match m {
            Msg::Up => {
                self.value.replace_with(|v| *v + 1);
            }
            Msg::Down => {
                self.value.replace_with(|v| *v - 1);
            }
        }
    }

    fn render_initial_view(&self, main: &WeakMain<Self>) -> Node {
        mika::dom::NodeList::new()
            .child(
                mika::dom::P::new()
                    .text("The initial value is ")
                    .text(&self.value.get().to_string()),
            )
            .child(
                mika::dom::Div::new()
                    .child(
                        mika::dom::Button::new()
                            .text("Down")
                            .on_click(main, |_| Msg::Down),
                    )
                    .text(" ")
                    .text_signal(self.value.signal())
                    .text(" ")
                    .child(
                        mika::dom::Button::new()
                            .text("Up")
                            .on_click(main, |_| Msg::Up),
                    ),
            )
            .child(mika::dom::P::new().update_websys_node_signal(
                self.value.signal(),
                |value, node| {
                    if value < 2019 {
                        node.set_text_content(Some(&format!("{} < 2019", value)))
                    } else if value > 2019 {
                        node.set_text_content(Some(&format!("2019 < {}", value)))
                    } else {
                        node.set_text_content(Some("It's 2019"))
                    }
                },
            ))
            .into()
    }
}
