use mika::dom::{
    Button, Div, Footer, Header, Input, InputType, Label, Li, NodeList, Section, Span, Strong, Ul,
    A, H1, P,
};
use mika::prelude::*;
use mika::update;
use signals::signal::{Mutable, SignalExt};
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;

mod todolist;

use todolist::*;

#[derive(Copy, Clone)]
struct Counter {
    pub all_item_count: usize,
    pub completed_item_count: usize,
}

impl Counter {
    fn is_all_completed(&self) -> bool {
        self.all_item_count > 0 && self.all_item_count == self.completed_item_count
    }
    fn is_empty(&self) -> bool {
        self.all_item_count == 0
    }
    fn item_left_count(&self) -> usize {
        self.all_item_count - self.completed_item_count
    }
    fn is_some_item_completed(&self) -> bool {
        self.completed_item_count > 0
    }
}

#[derive(Copy, Clone)]
struct Pair {
    id: u32,
    index: usize,
}

struct TodoApp {
    todo_list: TodoList,
    new_todo_id: std::cell::Cell<u32>,
    editing: Mutable<Option<Pair>>,

    // This is a hack to make life easier.
    // May be SignalVecExt must support more functionalities?
    // TODO: Open an issue on futures-signals about this?
    counter: Mutable<Counter>,
}

mika::create_app_handle!(TodoApp);

impl TodoApp {
    fn new() -> Self {
        Self {
            todo_list: TodoList::new(),
            new_todo_id: std::cell::Cell::new(0),
            editing: Mutable::new(None),
            counter: Mutable::new(Counter {
                all_item_count: 0,
                completed_item_count: 0,
            }),
        }
    }
}

enum Msg {
    New(web_sys::Event),
    Toggle(u32),
    ToggleAll(web_sys::Event),
    ClearCompleted,
    Remove(u32),
    StartEditing(u32),
    EndEditing(web_sys::KeyboardEvent),
    BlurEditing(web_sys::FocusEvent),
}

impl Application for TodoApp {
    fn init(_: &std::rc::Rc<Self>) {
        wasm_logger::init(wasm_logger::Config::new(log::Level::Debug));
    }
    fn render(app: &std::rc::Rc<Self>) -> Node {
        NodeList::new()
            .child(
                Section::new()
                    .class("todoapp")
                    .child(
                        Header::new()
                            .class("header")
                            .child(H1::new().text("Mika Todos"))
                            .child(
                                #[allow(deprecated)]
                                Input::new()
                                    .class("new-todo")
                                    // It is recommended to use label instead of placeholder,
                                    // so, input.placeholder is not implemented. We still set
                                    // placeholder for consistency with TodoMVC specs.
                                    //.string_attribute("placeholder", "What needs to be done?")
                                    .auto_focus(true)
                                    .on_change(update! {app, Msg::New(_)}),
                            ),
                    )
                    .child(
                        Section::new()
                            .class("main")
                            .class_signal(
                                "hidden",
                                app.counter.signal().map(|counter| counter.is_empty()),
                            )
                            .child(
                                Input::new()
                                    .id("toggle-all")
                                    .class("toggle-all")
                                    .r#type(&InputType::CheckBox)
                                    .checked_signal(
                                        app.counter
                                            .signal()
                                            .map(|counter| counter.is_all_completed()),
                                    )
                                    .on_change(update! {app, Msg::ToggleAll(_)}),
                            )
                            .child(
                                Label::new()
                                    .r#for("toggle-all")
                                    .text("Mark all as complete"),
                            )
                            .child(TodoApp::render_todo_list(app)),
                    )
                    .child(
                        Footer::new()
                            .class("footer")
                            .class_signal(
                                "hidden",
                                app.counter.signal().map(|counter| counter.is_empty()),
                            )
                            .child(Span::new().class("todo-count").child(
                                Strong::new().text_signal(app.counter.signal().map(|counter| {
                                    let item_left = counter.item_left_count();
                                    format!(
                                        "{} {} left",
                                        item_left,
                                        if item_left == 1 { "item" } else { "items" }
                                    )
                                })),
                            ))
                            .child(
                                Ul::new()
                                    .class("filters")
                                    // TODO: Implement routing
                                    .child(Li::new().child(A::new().href("#/").text("All")))
                                    .child(
                                        Li::new().child(A::new().href("#/active").text("Active")),
                                    )
                                    .child(
                                        Li::new()
                                            .child(A::new().href("#/completed").text("Completed")),
                                    ),
                            )
                            .child(
                                Button::new()
                                    .class("clear-completed")
                                    .class_signal(
                                        "hidden",
                                        app.counter
                                            .signal()
                                            .map(|counter| !counter.is_some_item_completed()),
                                    )
                                    .on_click(update! {app, Msg::ClearCompleted})
                                    .text("Clear completed"),
                            ),
                    ),
            )
            .child(
                Footer::new()
                    .class("info")
                    .child(P::new().text("Double-click to edit a todo"))
                    .child(P::new().text("Created by Limira as an example for the Mika framework"))
                    .child(
                        P::new()
                            .text("Part of ")
                            .child(A::new().href("http://todomvc.com").text("TodoMVC")),
                    ),
            )
            .into()
    }
}

impl TodoApp {
    fn render_todo_list(app: &std::rc::Rc<Self>) -> Ul {
        let app = app.clone();
        let cloned_editing = app.editing.clone();
        Ul::new().class("todo-list").list_signal(
            app.todo_list.list().signal_vec_cloned(),
            move |item| {
                let cloned_item = item.clone();
                let item_id = cloned_item.id;
                Li::new()
                    .class_signal("completed", item.completed.signal())
                    .class_signal(
                        "editing",
                        cloned_editing.signal().map(move |option| match option {
                            None => false,
                            Some(pair) => pair.id == item_id,
                        }),
                    )
                    .child(
                        Div::new()
                            .class("view")
                            .child(
                                Input::new()
                                    .class("toggle")
                                    .r#type(&InputType::CheckBox)
                                    .checked_signal(item.completed.signal())
                                    .on_change(update! {app, Msg::Toggle(item_id)}),
                            )
                            .child(
                                Label::new()
                                    .text_signal(item.title.signal_cloned())
                                    .on_double_click(update! {app, Msg::StartEditing(item_id)}),
                            )
                            .child(
                                Button::new()
                                    .class("destroy")
                                    .on_click(update! {app, Msg::Remove(item_id)}),
                            ),
                    )
                    .child(
                        Input::new()
                            .class("edit")
                            .value_signal(item.title.signal_cloned())
                            .focus_signal(cloned_editing.signal().map(move |option| match option {
                                None => false,
                                Some(pair) => pair.id == item_id,
                            }))
                            .on_blur(update! {app, Msg::BlurEditing(_)})
                            .on_key_press(update! {app, Msg::EndEditing(_)}),
                    )
            },
        )
    }

    fn next_id(&self) -> u32 {
        self.new_todo_id.set(self.new_todo_id.get() + 1);
        self.new_todo_id.get()
    }

    fn add_new_todo(&self, event: web_sys::Event) {
        if let Some(target) = event.target() {
            let input: &web_sys::HtmlInputElement = target.unchecked_ref();
            let title = input.value();
            let title = title.trim();
            if title.is_empty() {
                return;
            }
            let id = self.next_id();
            self.todo_list.add(id, title);
            input.set_value("");
        }
    }

    fn toggle_all(&self, event: web_sys::Event) {
        if let Some(target) = event.target() {
            let input: &web_sys::HtmlInputElement = target.unchecked_ref();
            self.todo_list.toggle_all(input.checked());
        }
    }

    fn start_editing(&self, id: u32) {
        self.editing.set(Some(Pair {
            id,
            index: self.todo_list.find_index(id),
        }))
    }

    fn end_editing(&self, event: web_sys::KeyboardEvent) {
        match event.key().as_str() {
            "Escape" => {
                self.editing.set(None);
            }
            "Enter" => {
                if let Some(target) = (event.as_ref() as &web_sys::Event).target() {
                    self.process_end_editing(target);
                }
            }
            _ => {}
        }
    }

    fn process_end_editing(&self, target: web_sys::EventTarget) {
        let input: &web_sys::HtmlInputElement = target.unchecked_ref();
        let title = input.value();
        let title = title.trim();
        if title.is_empty() {
            if let Some(pair) = self.editing.get() {
                self.todo_list.remove_by_index(pair.index);
                self.editing.set(None);
            }
        } else {
            if let Some(pair) = self.editing.get() {
                self.todo_list.update_title(pair.index, title.to_string());
                self.editing.set(None);
            }
        }
    }

    fn blur_editing(&self, event: web_sys::FocusEvent) {
        if let Some(target) = (event.as_ref() as &web_sys::Event).target() {
            self.process_end_editing(target);
        }
    }

    fn update(&self, m: Msg) {
        match m {
            Msg::New(event) => self.add_new_todo(event),
            Msg::Toggle(id) => self.todo_list.toggle(id),
            Msg::Remove(id) => self.todo_list.remove_by_id(id),
            Msg::ToggleAll(event) => self.toggle_all(event),
            Msg::ClearCompleted => self.todo_list.clear_completed(),
            Msg::StartEditing(index) => return self.start_editing(index),
            Msg::EndEditing(event) => self.end_editing(event),
            Msg::BlurEditing(event) => self.blur_editing(event),
        }
        self.counter.set(self.todo_list.counter());
    }
}
