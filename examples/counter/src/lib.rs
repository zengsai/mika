use mika::prelude::*;
use signals::signal::Mutable;
use wasm_bindgen::prelude::*;

struct Counter {
    value: Mutable<i32>,
}

mika::create_app_handle!(Counter);

impl Counter {
    fn new() -> Self {
        Self {
            value: Mutable::new(2019),
        }
    }
    fn up(&self) {
        self.value.replace_with(|x| *x + 1);
    }
    fn down(&self) {
        self.value.replace_with(|x| *x - 1);
    }
}

impl Application for Counter {
    fn render(app: &std::rc::Rc<Self>) -> Node {
        use mika::handler;
        mika::dom::NodeList::new()
            .child(
                mika::dom::P::new()
                    .text("The initial value is ")
                    .text(&app.value.get().to_string()),
            )
            .child(
                mika::dom::Div::new()
                    .child(
                        mika::dom::Button::new()
                            .text("Down")
                            .on_click(handler! {app.down()}),
                    )
                    .text(" ")
                    .text_signal(app.value.signal())
                    .text(" ")
                    .child(
                        mika::dom::Button::new()
                            .text("Up")
                            .on_click(handler! {app.up()}),
                    ),
            )
            .into()
    }
}
