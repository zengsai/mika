/// Create a handler from a method call on the app state. 'handler!` will
/// clone the app state and create a moved closure that wraps your state's method
/// call. The result is intended to be passed to an event register method
/// such as `.on_click`.
///
/// ## Example
/// ```rust
/// struct AppState;
/// impl AppState {
///     fn do_something(&self) { }
/// }
/// impl Application for AppState {
///     fn render(app: &std::rc::Rc<Self>) -> Node {
///         mika::dom::Button::new()
///             .on_click(
///                 mika::handler!{ app.do_something() }
///             )
///             .into()
///     }
/// }
/// ```
/// If the app's state method want receive the event argument, it must be the first
/// argument of the method. Then, you call `handler!` with the event argument
/// placeholder: `_`.
/// ## Example
/// ```rust
/// impl AppState {
///     fn do_with_event_arg(&self, event: web_sys::MouseEvent) {
///         // Do something with `event`
///     }
/// }
/// impl Application for AppState {
///     fn render(app: &std::rc::Rc<Self>) -> Node {
///         mika::dom::Input::new()
///             .on_change(
///                 mika::handler!{ app.do_with_event_arg(_) }
///             )
///             .into()
///     }
/// }
/// ```
/// If the method wants an extra argument, the you must make sure the argument to be
/// able to be moved into the handler (usually, it must not a reference).
/// ## Example
/// ```rust
/// impl AppState {
///     fn do_with_event_arg_and_extra_arg(&self, event: web_sys::MouseEvent, extra: i32) {
///         // Do something with `event` and `extra`
///     }
/// }
/// impl Application for AppState {
///     fn render(app: &std::rc::Rc<Self>) -> Node {
///         let the_extra_value = 42;
///         mika::dom::Input::new()
///             .on_change(
///                 mika::handler!{ app.do_with_event_arg_and_extra_arg(_, the_extra_value) }
///             )
///             .into()
///     }
/// }
/// ```

#[macro_export]
macro_rules! handler {
    ($state:ident . $($idents:ident).+ (_)) => {{
        let cloned_state = $state.clone();
        move |arg| cloned_state.$($idents).+(arg)
    }};
    ($state:ident . $($idents:ident).+ (_, $($tt:tt)+)) => {{
        let cloned_state = $state.clone();
        move |arg| cloned_state.$($idents).+(arg, $($tt)+)
    }};
    ($state:ident . $($idents:ident).+ ($($tt:tt)*)) => {{
        let cloned_state = $state.clone();
        move |_| cloned_state.$($idents).+($($tt)*)
    }};
}

/// Similar in functionality to `handler!`. But the resulted handler will call to
/// a special app-state's method named `update`. The `update` method must receive a
/// single argument, which is an enum.
/// ## Example
/// ```rust
/// struct AppState;
/// enum Msg {
///     Foo,
///     Bar(web_sys::Event),
///     Baz(u32),
/// }
/// impl AppState {
///     fn update(&self, m: Msg) {
///         // Do something with `m`
///     }
/// }
/// impl Application for AppState {
///     fn render(app: &std::rc::Rc<Self>) -> Node {
///         let id = 42;
///         mika::dom::Input::new()
///             .on_change(
///                 mika::update!{ app, Msg::Foo }
///                 // or mika::update!{ app, Msg::Bar(_) }
///                 // or mika::update!{ app, Msg::Baz(id) }
///             )
///             .into()
///     }
/// }
/// ```

#[macro_export]
macro_rules! update {
    ($state:ident, $first:ident$(::$idents:ident)*) => {{
        let cloned_state = $state.clone();
        move |_| cloned_state.update($first$(::$idents)*)
    }};
    ($state:ident, $first:ident$(::$idents:ident)*(_)) => {{
        let cloned_state = $state.clone();
        move |arg| cloned_state.update($first$(::$idents)*(arg))
    }};
    ($state:ident, $first:ident$(::$idents:ident)*(_, $($tt:tt)+)) => {{
        let cloned_state = $state.clone();
        move |arg| cloned_state.update($first$(::$idents)*(arg, $($tt)+))
    }};
    ($state:ident, $first:ident$(::$idents:ident)*($($tt:tt)+)) => {{
        let cloned_state = $state.clone();
        move |_| cloned_state.update($first$(::$idents)*($($tt)+))
    }};
}

/// Create a handle for your app named `AppHandle`. The app's handle will be export by #[wasm-bindgen].
/// In JavaScript, you can start your app by construct a new instance of AppHandle:
/// ```
/// window.my_app = new AppHandle();
/// ```
/// then, your app will be up and run.
#[macro_export]
macro_rules! create_app_handle {
    ($TypeName:ident) => {
        #[wasm_bindgen]
        pub struct AppHandle {
            _app: App<$TypeName>,
        }

        #[wasm_bindgen]
        impl AppHandle {
            #[wasm_bindgen(constructor)]
            pub fn new() -> Self {
                Self {
                    _app: App::start_in_body($TypeName::new()),
                }
            }
        }
    };
}

#[cfg(test)]
mod test {
    enum Msg {
        N42,
        Double(i32),
        Add(i32, i32),
        Sum(i32, i32, i32),
    }
    #[derive(Clone)]
    struct Test;
    impl Test {
        fn n42(&self) -> i32 {
            42
        }
        fn double(&self, me: i32) -> i32 {
            me * 2
        }
        fn add(&self, a: i32, b: i32) -> i32 {
            a + b
        }
        fn sum(&self, a: i32, b: i32, c: i32) -> i32 {
            a + b + c
        }
        fn update(&self, m: Msg) -> i32 {
            match m {
                Msg::N42 => 42,
                Msg::Double(value) => value * 2,
                Msg::Add(v1, v2) => v1 + v2,
                Msg::Sum(v1, v2, v3) => v1 + v2 + v3,
            }
        }
    }

    #[test]
    fn handler_no_arg() {
        assert_eq!(42, handler!(Test.n42())(()));
    }

    #[test]
    fn handler_with_arg() {
        assert_eq!(84, handler!(Test.double(42))(()));
        assert_eq!(84, handler!(Test.add(42, 42))(()));
        assert_eq!(85, handler!(Test.sum(42, 42, 1))(()));
    }

    #[test]
    fn handler_optional_arg_only() {
        assert_eq!(84, handler!(Test.double(_))(42));
    }

    #[test]
    fn handler_optional_arg_and_other() {
        assert_eq!(84, handler!(Test.add(_,42))(42));
        assert_eq!(85, handler!(Test.sum(_,42,1))(42));
    }

    #[test]
    fn update() {
        assert_eq!(42, update!(Test, Msg::N42)(()));
        assert_eq!(84, update!(Test, Msg::Double(42))(()));
        assert_eq!(84, update!(Test, Msg::Add(42, 42))(()));
        assert_eq!(80, update!(Test, Msg::Sum(42, 42, -4))(()));
    }

    #[test]
    fn update_with_optional_arg() {
        assert_eq!(84, update!(Test, Msg::Double(_))(42));
        assert_eq!(84, update!(Test, Msg::Add(_, 42))(42));
        assert_eq!(80, update!(Test, Msg::Sum(_, 42, 42))(-4));
    }
}
