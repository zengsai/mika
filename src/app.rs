use wasm_bindgen::UnwrapThrowExt;

pub trait Application {
    fn init(_app: &std::rc::Rc<Self>) {}
    fn render(app: &std::rc::Rc<Self>) -> crate::dom::Node;
    fn mounted(_app: &std::rc::Rc<Self>) {}
}

pub struct App<A: Application> {
    app: std::rc::Rc<A>,
    node: crate::dom::Node,
}

impl<A: Application> App<A> {
    pub fn start_in_body(app: A) -> Self {
        let app = std::rc::Rc::new(app);
        A::init(&app);
        let node = A::render(&app);
        let body = crate::document()
            .body()
            .expect_throw("crate::document().body()");
        body.set_inner_html("");
        node.append_to(body.as_ref());
        Self { app, node }
    }

    pub fn app(&self) -> &std::rc::Rc<A> {
        &self.app
    }

    pub fn node(&self) -> &crate::dom::Node {
        &self.node
    }
}
