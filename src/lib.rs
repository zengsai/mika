#[cfg(feature = "message-like-elm")]
mod app_with_message_like_elm;
#[cfg(feature = "message-like-elm")]
use app_with_message_like_elm as app;

#[cfg(not(feature = "message-like-elm"))]
mod app;

#[macro_use]
pub mod dom;
pub mod events;
#[macro_use]
pub mod macros;

use wasm_bindgen::UnwrapThrowExt;

pub mod prelude {
    pub use crate::app::*;
    pub use crate::dom::traits::*;

    pub use crate::dom::{HasChildren, Node};
    pub use simi_macros::simi_app;
}

pub fn window() -> web_sys::Window {
    web_sys::window().expect_throw("web_sys::window()")
}

pub fn document() -> web_sys::Document {
    crate::window()
        .document()
        .expect_throw("crate::window().document()")
}

#[cfg(feature = "message-like-elm")]
pub fn interval<A, F>(
    ms: u32,
    main: &crate::app::WeakMain<A>,
    handler: F,
) -> timers::callback::Interval
where
    A: crate::app::Application,
    F: Fn() -> A::Message + 'static,
{
    let main = main.clone();
    timers::callback::Interval::new(ms, move || main.send_message(handler()))
}
