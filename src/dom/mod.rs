/*!
Mika does not support deprecated or non standard html tags
*/
use signals::signal::SignalExt;
use signals::signal_vec::{SignalVecExt, VecDiff};
use wasm_bindgen::{JsCast, UnwrapThrowExt};

#[macro_use]
mod macros;

pub mod traits;
use traits::*;

fn create_element(tag: &str) -> web_sys::Element {
    crate::document()
        .create_element(tag)
        .expect_throw("create_element(tag)")
}

fn create_text_node(text: &str) -> web_sys::Text {
    crate::document().create_text_node(text)
}

// TODO: Store all future from signal and stop them when require
fn spawn_for_each<S, F>(signal: S, f: F)
where
    S: signals::signal::Signal + 'static,
    F: Fn(S::Item) + 'static,
{
    let f = signal.for_each(move |value| {
        f(value);
        futures_util::future::ready(())
    });
    wasm_bindgen_futures::futures_0_3::spawn_local(f);
}

// TODO: Store all future from signal and stop them when require
fn spawn_for_each_vec<S, F>(signal: S, f: F)
where
    S: signals::signal_vec::SignalVec + 'static,
    F: Fn(VecDiff<S::Item>) + 'static,
{
    let f = signal.for_each(move |value| {
        f(value);
        futures_util::future::ready(())
    });
    wasm_bindgen_futures::futures_0_3::spawn_local(f);
}

impl<T: Element> GlobalAttributes for T {}

define_helper_enums! {
    InputType {
        Button => "button",
        CheckBox => "checkbox",
        Color => "color",
        Date => "date",
        DateTimeLocal => "datetime-local",
        Email => "email",
        File => "file",
        Hidden => "hidden",
        Image => "image",
        Month => "month",
        Number => "number",
        Password => "password",
        Radio => "radio",
        Range => "range",
        Reset => "reset",
        Search => "search",
        Submit => "submit",
        Tel => "tel",
        Text => "text",
        Time => "time",
        Url => "url",
        Week => "week",
    }
    AutoCapitalize {
        Off => "off",
        On => "on",
        Sentences => "sentences",
        Words => "words",
        Characters => "characters",
    }
    Dir {
        LeftToRight => "ltr",
        RightToLeft => "rtl",
        Auto => "auto",
    }
    InputMode {
        None => "none",
        Text => "text",
        Decimal => "decimal",
        Numeric => "numeric",
        Tel => "tel",
        Search => "search",
        Email => "email",
        Url => "url",
    }
    Target {
        _Self => "_self",
        _Blank => "_blank",
        _Parent => "_parent",
        _Top => "_top",
    }
    PreLoad {
        None => "none",
        MetaData => "metadata",
        Auto => "auto",
    }
    Enctype {
        ApplicationXWwwFormUrlEncoded => "application/x-www-form-urlencoded",
        MultiPartFormData => "multipart/form-data",
        TextPlain => "text/plain",
    }
    FormMethod {
        Post => "post",
        Get => "get",
    }
    AutoComplete {
        On => "on",
        Off => "off",
    }
    ReferrerPolicy {
        NoReferrer => "no-referrer",
        NoReferrerWhenDowngrade => "no-referrer-when-downgrade",
        Origin => "origin",
        OriginWhenCrossOrigin => "origin-when-cross-origin",
        SameOrigin => "same-origin",
        StrictOrigin => "strict-origin",
        StrictOriginWhenCrossOrigin => "strict-origin-when-cross-origin",
        UnsafeUrl => "unsafe-url",
    }
    Sandbox {
        AllowForms => "allow-forms",
        AllowModals => "allow-modals",
        AllowOrientationLock => "allow-orientation-lock",
        AllowPointerLock => "allow-pointer-lock",
        AllowPopups => "allow-popups",
        AllowPopupsToEscapeSandbox => "allow-popups-to-escape-sandbox",
        AllowPresentation => "allow-presentation",
        AllowSameOrigin => "allow-same-origin",
        AllowScripts => "allow-scripts",
        // Should we have feature = "experimental" for things like this?
        // allow-storage-access-by-user-activation
        AllowTopNavigation => "allow-top-navigation",
        AllowTopNavigationByUserActivation => "allow-top-navigation-by-user-activation",
        //AllowDownloadsWithoutUserActivation => "allow-downloads-without-user-activation",
    }
    CrossOrigin {
        Anonymous => "anonymous",
        UseCredentials => "use-credentials",
    }
    Decoding {
        Sync => "sync",
        Async => "async",
        Auto => "auto",
    }
    OlType {
        Number => "1",
        LowerCase => "a",
        UpperCase => "A",
        LowerCaseRoman => "i",
        UpperCaseRoman => "I",
    }
    SpellCheck {
        True => "true",
        False => "false",
        Default => "default",
    }
    Wrap {
        Hard => "hard",
        Soft => "soft",
    }
    ThScope {
        Row => "row",
        Col => "col",
        RowGroup => "rowgroup",
        ColGroup => "colgroup",
        Auto => "auto",
    }
    TrackKind {
        Subtitles => "subtitles",
        Captions => "captions",
        Descriptions => "descriptions",
        Chapters => "chapters",
        Metadata => "metadata",
    }
}

/// If you don't want to use this struct, construct your own
/// string and set it by .bool_attribute("coords", ...)
pub enum Coords {
    Rect {
        left: i32,
        top: i32,
        right: i32,
        bottom: i32,
    },
    Circle {
        x: i32,
        y: i32,
        radius: i32,
    },
    // TODO a good way to represent a polygon?
    Poly(String),
}
impl Coords {
    // TODO impl Display instead?
    pub fn to_string(&self) -> String {
        match self {
            Coords::Rect {
                left,
                top,
                right,
                bottom,
            } => format!("{},{},{},{}", left, top, right, bottom),
            Coords::Circle { x, y, radius } => format!("{},{},{}", x, y, radius),
            Coords::Poly(s) => s.clone(),
        }
    }
}

define_html_elements! {
    // See ChildOfA in traits.rs
    a A [ChildOfA],
    abbr Abbr [PhrasingContent],
    // TODO How about special conditions?
    address Address [FlowContent],
    // area is an empty element
    area Area,
    article Article [FlowContent],
    aside Aside [FlowContent],
    audio Audio [ChildOfAudioVideo],
    b B [PhrasingContent],
    bdi Bdi [PhrasingContent],
    bdo Bdo [PhrasingContent],
    blockquote BlockQuote [FlowContent],
    // br is an empty element
    br Br,
    // TODO PhrasingNonInteractiveContent must not appear as descendant as well
    button Button [PhrasingNonInteractiveContent],
    // TODO Transparent but with no interactive content descendants except for <a> elements,
    // <button> elements, <input> elements whose type attribute is checkbox, radio, or button.
    canvas Canvas [Element],
    caption Caption [FlowContent],
    cite Cite [PhrasingContent],
    code Code [PhrasingContent],
    // col is an empty element
    col Col,
    colgroup ColGroup [ChildOfColGroup],
    data Data [PhrasingContent],
    datalist DataList [ImOption],
    dd Dd [FlowContent],
    // TODO Transparent?
    // Allow everything for now
    // See also ins
    del Del [Element],
    // TODO One <summary> element followed by flow content.
    details Details [ChildOfDetails],
    // TODO No dfn descendant
    dfn Dfn [PhrasingContent],
    dialog Dialog [FlowContent],
    div Div [FlowContent],
    // TODO Either
    // * (dt)+ (dd)+
    // * (div)+
    dl Dl [ChildOfDl],
    dt Dt [FlowContent],
    em Em [PhrasingContent],
    // embed is an empty element
    embed Embed,
    fieldset FieldSet [ChildOfFieldSet],
    figcaption FigCaption [FlowContent],
    // TODO Replace ChildOfFigure by FlowContent and TraitCaption?
    figure Figure [ChildOfFigure],
    // TODO How about no header or no footer descendant?
    footer Footer [FlowContent],
    // TODO Not contains a form
    form Form [FlowContent],
    h1 H1 [PhrasingContent],
    h2 H2 [PhrasingContent],
    h3 H3 [PhrasingContent],
    h4 H4 [PhrasingContent],
    h5 H5 [PhrasingContent],
    h6 H6 [PhrasingContent],
    // TODO How about no header or no footer descendant?
    header Header [FlowContent],
    hgroup Hgroup [Headings],
    // hr is an empty element
    hr Hr,
    i I [PhrasingContent],
    // TODO: Fallback content?
    // Allow everything now
    iframe Iframe [Element],
    // img is an empty element
    img Img,
    // input is an empty element
    input Input,
    // TODO Transparent?
    // Allow everything for now
    // See also del
    ins Ins [Element],
    kbd Kbd [PhrasingContent],
    // TODO No label descendant or actually FormLabelableContent?
    label Label [PhrasingContent],
    legend Legend [PhrasingContent],
    li Li [FlowContent],
    main Main [FlowContent],
    // TODO Any transparent element.?
    // https://developer.mozilla.org/en-US/docs/Web/HTML/Element/map
    map Map [Element],
    mark Mark [PhrasingContent],
    // TODO <math>'s children
    [mdn = "https://developer.mozilla.org/en-US/docs/Web/MathML/Element/math"]
    math Math,
    // This is an experimental technology. Check the Browser compatibility table carefully before using this in production.
    // Not support now.
    // ==========> menu Menu,
    // Deprecated since HTML5.2
    // ==========> menuitem MenuItem,
    // TODO No meter descendant
    meter Meter [PhrasingContent],
    nav Nav [FlowContent],
    // TODO Does this need to be supported in mika?
    // Will mika support for SSR?
    // ==========> noscript NoScript [Element],
    // TODO zero or more <param> elements, then transparent.
    object Object [Element],
    ol Ol [ImLi],
    optgroup OptGroup [ImOption],
    // Text only, TextContent is impled for Option
    option Option,
    output Output [PhrasingContent],
    p P [PhrasingContent],
    // param is an empty element
    param Param,
    // Replace ChildOfPicture with TraitPicture and TraitSource?
    picture Picture [ChildOfPicture],
    pre Pre [PhrasingContent],
    // TODO no progress descendant
    progress Progress [PhrasingContent],
    q Q [PhrasingContent],
    // Text only, TextContent is impled for Rp
    rp Rp,
    rt Rt [PhrasingContent],
    rtc Rtc [ChildOfRtc],
    ruby Ruby [PhrasingContent],
    s S [PhrasingContent],
    samp Samp [PhrasingContent],
    // ==========> script Script, Not support in mika
    section Section [FlowContent],
    select Select [ChildOfSelect],
    // TODO Transparent?
    slot Slot [Element],
    small Small [PhrasingContent],
    // source is an empty element
    source Source,
    span Span [PhrasingContent],
    strong Strong [PhrasingContent],
    sub Sub [PhrasingContent],
    summary Summary [ChildOfSummary],
    sup Sup [PhrasingContent],
    // TODO <svg>'s children
    [mdn = "https://developer.mozilla.org/en-US/docs/Web/SVG/Element/svg"]
    svg Svg,
    table Table [ChildOfTable],
    tbody Tbody [ImTr],
    td Td [FlowContent],
    template Template [Element],
    // impled text by TraitTextArea
    textarea TextArea,
    tfoot Tfoot [ImTr],
    // TODO Flow content, but with no header, footer, sectioning content, or heading content descendants.
    th Th [FlowContent],
    thead Thead [ImTr],
    time Time [PhrasingContent],
    // TODO Zero or more <td> and/or <th> elements; script-supporting elements (<script> and <template>) are also allowed
    tr Tr [ChildOfTr],
    // tract is an empty element
    track Track,
    u U [PhrasingContent],
    ul Ul [ImLi],
    var Var [PhrasingContent],
    video Video [ChildOfAudioVideo],
    // Empty element
    wbr Wbr
}

implement_marker_trait_for! {
    // Category traits
    EmbeddedContent { Audio Canvas Embed Iframe Img Math Object Picture Svg Video }
    FlowContent { A Abbr Address Article Aside Audio B Bdi Bdo BlockQuote Br Button
        Canvas Cite Code Data DataList Del Details Dfn Div Dl
        Em Embed FieldSet Figure Footer Form
        H1 H2 H3 H4 H5 H6 Header Hgroup Hr
        I Iframe Img Input Ins Kbd Label
        Main Map Mark Math /*Menu*/ Meter Nav /*NoScript*/
        Object Ol Output P Picture Pre Progress Q Ruby
        S Samp /*Script*/ Section Select Small Span Strong Sub Sup Svg
        Table Template TextArea Time
        U Ul Var Video Wbr
    }
    FlowNonInteractiveContent { Abbr Address Article Aside Audio
        B Bdi Bdo BlockQuote Br Canvas Cite Code
        Data DataList Del Dfn Div Dl Em FieldSet Figure Footer Form
        H1 H2 H3 H4 H5 H6 Header Hgroup Hr I Img Input Ins Kbd
        Main Map Mark Math /*Menu*/ Meter Nav /*NoScript*/
        Object Ol Output P Picture Pre Progress Q Ruby
        S Samp /*Script*/ Section Small Span Strong Sub Sup Svg
        Table Template Time U Ul Var Video Wbr
    }
    FormContent { Button FieldSet Input Label Meter Object Output Progress Select TextArea }
    FormListedContent { Button FieldSet Input Object Output Select TextArea }
    FormLabelableContent { Button Input Meter Output Progress Select TextArea }
    FormSubmittableContent { Button Input Object Select TextArea }
    FormResettableContent { Input Output Select TextArea }
    HeadingContent { H1 H2 H3 H4 H5 H6 Hgroup }
    // TODO: audio, img, input, menu, object, video
    InteractiveContent { A Button Details Embed Iframe Label Select TextArea }
    //MetadataContent { NoScript Script}
    PhrasingContent {
        // TODO: link, meta?
        // TODO: This first line contains elements that is phrasing content with a specific condition
        A Area Del Ins Map
        Abbr Audio B Bdo Br Button Canvas Cite Code
        Data DataList Dfn Em Embed I Iframe Img Input
        Kbd Label Mark Math Meter /*NoScript*/ Object Output Picture Progress Q Ruby
        Samp /*Script*/ Select Small Span Strong Sub Svg TextArea Time U Var Video
    }
    PhrasingNonInteractiveContent {
        Abbr Audio B Bdo Br Canvas Cite Code
        Data DataList Dfn Em I Img Input
        Kbd Mark Math Meter /*NoScript*/ Object Output Picture Progress Q Ruby
        Samp /*Script*/ Small Span Strong Sub Svg Time U Var Video
    }
    SectioningContent { Article Aside Nav Section }
    // Special traits
    TextContent { Option Rp }
    Headings { H1 H2 H3 H4 H5 H6 }
    ImLi { Li }
    ImOption { Option }
    ImTr { Tr }
    // ChildOfXXX
    ChildOfAudioVideo { Source Track }
    ChildOfColGroup { Col }
    ChildOfDetails { Summary }
    ChildOfDl { Div Dd Dt }
    ChildOfFieldSet { Legend }
    ChildOfFigure { FigCaption }
    ChildOfPicture { Img Source }
    ChildOfRtc { Rt }
    ChildOfSelect { Option OptGroup }
    ChildOfSummary { H1 H2 H3 H4 H5 H6 Hgroup }
    ChildOfTable { Caption ColGroup Thead Tbody Tr Tfoot }
    ChildOfTr { Td Th }
}

// TODO more specialized types, such as: hreflang: HrefLang?
implement_attributes! {
    A {
        download: string,
        href: string,
        // TODO use same Lang type as GlobalAttributes::lang?
        hreflang href_lang: string,
        ping: string,
        // TODO LinkType?
        rel: string,
        target: string (Target),
        // TODO Mime type?
        type r#type: string,
    }
    Area {
        alt: string,
        // coords: string, specialized
        download: string,
        href: string,
        // TODO use same Lang type as GlobalAttributes::lang?
        hreflang href_lang: string,
        ping: string,
        // TODO LinkType?
        rel: string,
        target: string (Target),
    }
    Audio {
        autoplay auto_play: bool,
        controls: bool,
        crossorigin cross_origin: string (CrossOrigin),
        loop r#loop: bool,
        muted: bool,
        preload pre_load: string (PreLoad),
        src: string,
    }
    BlockQuote {
        // TODO Url?
        cite: string,
    }
    Button {
        autofocus auto_focus: bool,
        disabled: bool,
        form: string,
        formaction form_action: string,
        formenctype form_enctype: string (Enctype),
        formmethod form_method: string (FormMethod),
        formnovalidate form_no_validate: bool,
        formtarget form_target: string (Target),
        name: string,
        value: string,
    }
    Canvas {
        height: u32,
        width: u32,
    }
    Col {
        span: u32,
    }
    ColGroup {
        span: u32,
    }
    Data {
        value: string,
    }
    Del {
        // TODO ?
        cite: string,
        // TODO ?
        date_time: string,
    }
    Details {
        open: bool,
    }
    Dialog {
        open: bool,
    }
    Embed {
        height: u32,
        // TODO
        source: string,
        // TODO
        type r#type: string,
        width: u32,
    }
    FieldSet {
        disabled: bool,
        form: string,
        name: string,
    }
    Form {
        // TODO The current macro implementation only accept attribute names
        // that are valid Rust's identifiers. `accept-charset` is not valid.
        // ======> accept-charset
        // TODO Uri?
        action: string,
        autocomplete auto_complete: string (AutoComplete),
        enctype: string (Enctype),
        // TODO this method include `dialog` which is not available for button.formmethod?
        method: string (FormMethod),
        novalidate no_validate: bool,
        // TODO: this target include `iframename`?
        target: string (Target),
    }
    Iframe {
        allow: string,
        // use allow="fullscreen" instead
        // allowfullscreen allow_fullscreen: bool,
        // use allow="payment" instead
        // allowpaymentrequest
        height: u32,
        name: string,
        referrerpolicy referrer_policy: string (ReferrerPolicy),
        // sandbox: string, // specialized
        // TODO
        src: string,
        srcdoc src_doc: string,
        width: u32,
    }
    Img {
        alt: string,
        crossorigin cross_origin: string (CrossOrigin),
        decoding: string (Decoding),
        height: u32,
        // importance: experimental
        // intrinsicsize
        ismap is_map: bool,
        // referrerpolicy
        sizes: string,
        src: string,
        srcset src_set: string,
        width: u32,
        usemap use_map: string,
    }
    Input {
        // TODO ?
        autocomplete auto_complete: string,
        autofocus auto_focus: bool,
        disabled: bool,
        form: string,
        list: string,
        name: string,
        // placeholder: use label instead
        readonly read_only: bool,
        required: bool,
        // tabindex is it global?
        type r#type: string (InputType),
        // value: specialized
    }
    Ins {
        // TODO ?
        cite: string,
        // TODO ?
        date_time: string,
    }
    Label {
        for r#for: string,
        form: string,
    }
    Li {
        value: string,
    }
    Map {
        name: string,
    }
    // TODO
    // Math {
    // }
    Meter {
        // Should these be float?
        value: u32,
        min: u32,
        max: u32,
        low: u32,
        high: u32,
        optimum: u32,
        form: string,
    }
    Object {
        // TODO
        form: string,
        height: u32,
        name: string,
        // TODO
        type r#type: string,
        typemustmatch type_must_match: bool,
        usemap: string,
        width: u32,
    }
    Ol {
        reversed: bool,
        start: u32,
        type r#type: string (OlType),
    }
    OptGroup {
        disabled: bool,
        label: string,
    }
    Option {
        disabled: bool,
        label: string,
        selected: bool,
        value: string,
    }
    Output {
        for r#for: string,
        form: string,
        name: string,
    }
    Param {
        name: string,
        value: string,
    }
    Progress {
        max: u32,
        value: u32,
    }
    Q {
        // TODO
        cite: string,
    }
    Select {
        // TODO ?
        autocomplete auto_complete: string,
        autofocus auto_focus: bool,
        disabled: bool,
        form: string,
        multiple: bool,
        name: string,
        required: bool,
        size: u32,
    }
    Slot {
        name: string,
    }
    Source {
        sizes: string,
        src: string,
        srcset src_set: string,
        type r#type: string,
        media: string,
    }
    // TODO
    // Svg {
    // }
    Td {
        colspan col_span: u32,
        headers: string,
        rowspan row_span: u32,
    }
    TextArea {
        autocomplete auto_complete: string (AutoComplete),
        autofocus auto_focus: bool,
        cols: u32,
        disabled: bool,
        form: string,
        maxlength max_length: u32,
        minlength min_length: u32,
        name: string,
        // placeholder use label instead
        readonly read_only: u32,
        required: bool,
        rows: u32,
        spellcheck spell_check: string (SpellCheck),
        wrap: string (Wrap),
    }
    Th {
        abbr: string,
        colspan col_span: u32,
        headers: string,
        rowspan row_span: u32,
        scope: string (ThScope),
    }
    Time {
        datetime date_time: string,
    }
    Track {
        default: string,
        kind: string (TrackKind),
        label: string,
        src: string,
        srclang src_lang: string,
    }
    Video {
        autoplay auto_play: bool,
        buffered: string,
        controls: bool,
        crossorigin cross_origin: string (CrossOrigin),
        height: u32,
        loop r#loop: bool,
        muted: bool,
        preload pre_load: string (PreLoad),
        playsinline plays_inline: bool,
        poster: string,
        src: string,
        width: u32,
    }
}

impl Area {
    pub fn coords(self, value: &Coords) -> Self {
        self.string_attribute("coords", &value.to_string())
    }
}

impl Iframe {
    pub fn sandbox(self, allows: &[Sandbox]) -> Self {
        self.string_attribute(
            "sandbox",
            &allows
                .iter()
                .map(|i| i.as_str())
                .collect::<Vec<&str>>()
                .join(" "),
        )
    }
}

impl Input {
    pub fn value(self, value: &str) -> Self {
        self.websys_element()
            .unchecked_ref::<web_sys::HtmlInputElement>()
            .set_value(value);
        self
    }

    pub fn value_signal<S, T>(self, signal: S) -> Self
    where
        T: Clone + ToString + 'static,
        S: signals::signal::Signal<Item = T> + 'static,
    {
        let ws_input: web_sys::HtmlInputElement = self.websys_element().clone().unchecked_into();
        spawn_for_each(signal, move |value| {
            ws_input.set_value(&value.to_string());
        });
        self
    }
    pub fn checked(self, value: bool) -> Self {
        self.websys_element()
            .unchecked_ref::<web_sys::HtmlInputElement>()
            .set_checked(value);
        self
    }
    pub fn checked_signal<S>(self, signal: S) -> Self
    where
        S: signals::signal::Signal<Item = bool> + 'static,
    {
        let ws_input: web_sys::HtmlInputElement = self.websys_element().clone().unchecked_into();
        spawn_for_each(signal, move |value| {
            ws_input.set_checked(value);
        });
        self
    }
}

pub struct NodeList {
    nodes: Vec<Node>,
}

impl Attachable for NodeList {
    fn append_to(&self, parent: &web_sys::Node) {
        self.nodes.iter().for_each(|n| {
            n.append_to(parent);
        })
    }
    fn insert_at(&self, _: usize, _: &web_sys::Node) {
        log::info!("Attachable::insert_at(self: &NodeList) do nothing!");
    }
    fn replace_at(&self, _: usize, _: &web_sys::Node) {
        log::info!("Attachable::replace_at(self: &NodeList) do nothing!");
    }
    fn remove_from(&self, _: &web_sys::Node) {
        log::info!("Attachable::remove_from(self: &NodeList) do nothing!");
    }
}

impl NodeList {
    pub fn new() -> Self {
        Self { nodes: Vec::new() }
    }

    pub fn child<T>(mut self, child: T) -> Self
    where
        T: Into<Node>,
    {
        self.nodes.push(child.into());
        self
    }
}

impl Default for NodeList {
    fn default() -> Self {
        Self::new()
    }
}

impl From<NodeList> for Node {
    fn from(item: NodeList) -> Self {
        Node::NodeList(item)
    }
}

pub trait HasChildren: Element + Sized {
    fn node_list(&self) -> std::sync::Arc<std::sync::Mutex<NodeList>>;
    /// This is primarily for internal use only
    fn push_child<T>(self, child: T) -> Self
    where
        T: Attachable + Into<Node>,
    {
        child.append_to(self.websys_node());
        self.node_list()
            .lock()
            .expect_throw("self.node_list().lock()")
            .nodes
            .push(child.into());
        self
    }

    /// Create children for self from a SignalVec. You should not add any child via .child
    /// on the element you invoke this method.
    fn list_signal<S, T, F, N>(self, signal: S, render_item: F) -> Self
    where
        T: Clone + 'static,
        S: signals::signal_vec::SignalVec<Item = T> + 'static,
        F: Fn(T) -> N + 'static,
        N: crate::dom::Element + Into<crate::dom::Node>,
    {
        let parent_node = self.websys_node().clone();
        let child_list = self.node_list();

        debug_assert_eq!(
            0,
            child_list
                .lock()
                .expect_throw("child_list.lock()")
                .nodes
                .len()
        );

        spawn_for_each_vec(signal, move |change| {
            let mut child_list = child_list.lock().expect_throw("child_list.lock()");
            match change {
                VecDiff::Replace { values } => {
                    //log::info!("Replace all items");

                    parent_node.set_text_content(None);
                    child_list.nodes.clear();

                    values.into_iter().for_each(|item| {
                        let child = render_item(item);
                        child.append_to(&parent_node);
                        child_list.nodes.push(child.into());
                    });
                }
                VecDiff::Push { value } => {
                    //log::info!("Current item count: {}, push new item", child_list.nodes.len());

                    let child = render_item(value);
                    child.append_to(&parent_node);
                    child_list.nodes.push(child.into());
                }
                VecDiff::Pop {} => {
                    debug_assert!(!child_list.nodes.is_empty());

                    child_list
                        .nodes
                        .pop()
                        .expect_throw("child_list.nodes.pop()")
                        .remove_from(&parent_node);
                }
                VecDiff::InsertAt { index, value } => {
                    //log::info!("Insert new item at {}", index);

                    let child = render_item(value);
                    child.insert_at(index, &parent_node);
                    child_list.nodes.insert(index, child.into());
                }
                VecDiff::RemoveAt { index } => {
                    //log::info!("Current item count: {}, remove at {}", child_list.nodes.len(), index);

                    child_list.nodes.remove(index).remove_from(&parent_node);
                }
                VecDiff::UpdateAt { index, value } => {
                    //log::info!("Current item count: {}, update at {}", child_list.nodes.len(), index);
                    let child = render_item(value);
                    child.replace_at(index, &parent_node);
                }
                VecDiff::Move {
                    old_index,
                    new_index,
                } => {
                    let moved = child_list.nodes.remove(old_index);
                    // TODO: Does this need to be removed_from first?
                    // This desperately need a #[test]
                    moved.remove_from(&parent_node);
                    moved.insert_at(new_index, &parent_node);
                    child_list.nodes.insert(new_index, moved);
                }
                VecDiff::Clear {} => {
                    parent_node.set_text_content(None);
                    child_list.nodes.clear();
                }
            }
        });
        self
    }
}
