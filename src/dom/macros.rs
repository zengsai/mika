macro_rules! define_html_elements {
    ($(
        $([mdn=$doc_link:expr])? $html_element_tag:ident $HtmlElementType:ident $([$($tt:tt)+])?
    ),+) => {
        pub enum Node {
            $(
                $HtmlElementType($HtmlElementType),
            )+
            NodeList(NodeList),
        }
        impl Node {
            pub(crate) fn append_to(&self, parent: &web_sys::Node) {
                match self {
                    $(
                        Node::$HtmlElementType(element) => element.append_to(parent),
                    )+
                    Node::NodeList(list) => list.append_to(parent),
                }
            }

            pub(crate) fn insert_at(&self, index: usize, parent: &web_sys::Node) {
                match self {
                    $(
                        Node::$HtmlElementType(element) => element.insert_at(index, parent),
                    )+
                    Node::NodeList(_) => {
                        log::info!("Node::NodeList::remove_from do nothing");
                    }
                }
            }

            pub(crate) fn remove_from(&self, parent: &web_sys::Node) {
                match self {
                    $(
                        Node::$HtmlElementType(element) => element.remove_from(parent),
                    )+
                    Node::NodeList(_) => {
                        log::info!("Node::NodeList::remove_from do nothing");
                    }
                }
            }
        }
        $(
            define_html_elements!{ @doc $($doc_link)?, $html_element_tag $HtmlElementType $([$($tt)+])? }
        )+
    };
    ( @doc $doc_link:expr, $html_element_tag:ident $HtmlElementType:ident $([$($tt:tt)+])? ) => {
        define_html_elements!{ @one_element $doc_link, $html_element_tag $HtmlElementType $([$($tt)+])? }
    };
    ( @doc , $html_element_tag:ident $HtmlElementType:ident $([$($tt:tt)+])? ) => {
        define_html_elements!{ @one_element
            concat!("https://developer.mozilla.org/en-US/docs/Web/HTML/Element/", stringify!($html_element_tag)),
            $html_element_tag $HtmlElementType $([$($tt)+])?
        }
    };
    (@one_element $doc_link:expr, $html_element_tag:ident $HtmlElementType:ident) => {
        #[doc = $doc_link]
        pub struct $HtmlElementType {
            websys_element: web_sys::Element,
            listeners: Vec<Box<crate::events::Listener>>,
        }

        impl $HtmlElementType {
            pub fn new() -> Self {
                Self {
                    websys_element: create_element(stringify!($html_element_tag)),
                    listeners: Vec::new(),
                }
            }
        }

        impl Default for $HtmlElementType {
            fn default() -> Self {
                Self::new()
            }
        }

        impl From<$HtmlElementType> for Node {
            fn from(item: $HtmlElementType) -> Self {
                Node::$HtmlElementType(item)
            }
        }

        impl Element for $HtmlElementType {
            fn websys_element(&self) -> &web_sys::Element {
                &self.websys_element
            }

            fn websys_node(&self) -> &web_sys::Node {
                self.websys_element.as_ref()
            }

            fn store_future(&mut self) {
                //
            }

            fn listeners_mut(&mut self) -> &mut Vec<Box<crate::events::Listener>> {
                &mut self.listeners
            }
        }
    };
    (@one_element $doc_link:expr, $html_element_tag:ident $HtmlElementType:ident [$($tt:tt)+]) => {
        #[doc = $doc_link]
        pub struct $HtmlElementType {
            websys_element: web_sys::Element,
            listeners: Vec<Box<crate::events::Listener>>,
            node_list: std::sync::Arc<std::sync::Mutex<NodeList>>,
        }

        impl $HtmlElementType {
            pub fn new() -> Self {
                Self {
                    websys_element: create_element(stringify!($html_element_tag)),
                    listeners: Vec::new(),
                    node_list: std::sync::Arc::new(std::sync::Mutex::new(NodeList::new())),
                }
            }
            pub fn child<T: Attachable + Into<Node> + $($tt)+>(self, child: T) -> Self {
                self.push_child(child)
            }
        }

        impl Default for $HtmlElementType {
            fn default() -> Self {
                Self::new()
            }
        }

        impl From<$HtmlElementType> for Node {
            fn from(item: $HtmlElementType) -> Self {
                Node::$HtmlElementType(item)
            }
        }

        impl Element for $HtmlElementType {
            fn websys_element(&self) -> &web_sys::Element {
                &self.websys_element
            }

            fn websys_node(&self) -> &web_sys::Node {
                self.websys_element.as_ref()
            }

            fn store_future(&mut self) {
                //
            }

            fn listeners_mut(&mut self) -> &mut Vec<Box<crate::events::Listener>> {
                &mut self.listeners
            }
        }

        impl HasChildren for $HtmlElementType {
            fn node_list(&self) -> std::sync::Arc<std::sync::Mutex<NodeList>> {
                std::sync::Arc::clone(&self.node_list)
            }
        }
    };
}

macro_rules! implement_marker_trait_for {
    ($($TraitName:ident {$($ElementType:ident)+})+) => {
        $(
            $(
                impl $TraitName for $ ElementType {}
            )+
        )+
    }
}

macro_rules! define_attribute_method {
    // There may be a bug in rust where an empty $vis:vis cause error if this macro is invoked in a trait
    // Is it a bug? Need to open an issue on /rust-lang/rust?
    // Work around by using $([$vis:ident])?
    ($([$vis:ident])? $attribute_name:ident: $type:ident $(($($tt:tt)+))?) => {
        define_attribute_method!{ $([$vis])? $attribute_name $attribute_name: $type $(($($tt)+))? }
    };
    ($([$vis:ident])? $attribute_name:ident $method_name:ident: string ($($tt:tt)+)) => {
        $($vis)? fn $method_name(self, value: &$($tt)+) -> Self {
            self.string_attribute(stringify!($attribute_name), &value.as_str())
        }
    };
    ($([$vis:ident])? $attribute_name:ident $method_name:ident: string) => {
        $($vis)? fn $method_name(self, value: &str) -> Self {
            self.string_attribute(stringify!($attribute_name), value)
        }
    };
    ($([$vis:ident])? $attribute_name:ident $method_name:ident: bool) => {
        $($vis)? fn $method_name(self, value: bool) -> Self {
            self.bool_attribute(stringify!($attribute_name), value)
        }
    };
    ($([$vis:ident])? $attribute_name:ident $method_name:ident: u32) => {
        $($vis)? fn $method_name(self, value: u32) -> Self {
            self.u32_attribute(stringify!($attribute_name), value)
        }
    };
    ($([$vis:ident])? $attribute_name:ident $method_name:ident: i32) => {
        $($vis)? fn $method_name(self, value: i32) -> Self {
            self.i32_attribute(stringify!($attribute_name), value)
        }
    };
}

macro_rules! define_attribute_methods_in_trait {
    ($(
        $($idents:ident)+: $type:ident $(($($tt:tt)+))?,
    )+) => {
        $(
            define_attribute_method!{ $($idents)+: $type $(($($tt)+))? }
        )+
    }
}

macro_rules! implement_attributes {
    ($($ElementType:ident {
        $($($name:ident)+: $type:ident $(($($tt:tt)+))?,)+
    })+) => {
        $(
            impl $ElementType {
                $(
                    define_attribute_method!{ [pub] $($name)+: $type $(($($tt)+))? }
                )+
            }
        )+
    };
}

macro_rules! define_helper_enums {
    ($(
        $EnumTypeName:ident {$($VariantName:ident => $str_value:literal,)+}
    )+) => {
        $(
            pub enum $EnumTypeName {
                $(
                    $VariantName,
                )+
            }
            impl $EnumTypeName {
                fn as_str(&self) -> &'static str {
                    match self {
                        $(
                            $EnumTypeName::$VariantName => $str_value,
                        )+
                    }
                }
            }
        )+
    };
}
